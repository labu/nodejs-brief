const express = require( 'express' );
const router = express.Router();
const utils = require('../src/utils');
const results = require('../src/results');

router.get( '/',
    async (req, res) => {
        try {
            if ( !utils.validateKey( req.header('x-api-key') ) ) {
                throw 'unauthorised';
            }
            const data = await results.getAll();
            res.send( data );
        } catch (err) {
            if (err === 'unauthorised') {
                res.status( 401 ).send( 'Unauthorised' );
            } else {
                res.status( 500 ).send( err.toString() );
            }
        }

});

module.exports = router;