const express = require( 'express' );
const router = express.Router();

router.use( '/teams', require( './api.teams.router' ) );
router.use( '/players', require( './api.players.router' ) );
router.use( '/results', require( './api.results.router' ) );

module.exports = router;