const express = require( 'express' );
const router = express.Router();
const utils = require('../src/utils');
const players = require('../src/players');

router.get( '/',
    async (req, res) => {
        try {
            if ( !utils.validateKey( req.header('x-api-key') ) ) {
                throw 'unauthorised';
            }
            const data = await players.getAll();
            res.send( data );
        } catch (err) {
            if (err === 'unauthorised') {
                res.status( 401 ).send( 'Unauthorised' );
            } else {
                res.status( 500 ).send( err.toString() );
            }
        }
});

router.get( '/:ids',
    async (req, res) => {
        try {
            if ( !utils.validateKey( req.header('x-api-key') ) ) {
                throw 'unauthorised';
            }

            const ids = req.params.ids ? req.params.ids.split(',') : [];
            const data = await players.getByIds(ids);
            res.send( data );
        } catch (err) {
            if (err === 'unauthorised') {
                res.status( 401 ).send( 'Unauthorised' );
            } else {
                res.status( 500 ).send( err.toString() );
            }
        }
});

router.get( '/:ids/stats',
    async (req, res) => {
        try {
            if ( !utils.validateKey( req.header('x-api-key') ) ) {
                throw 'unauthorised';
            }

            const ids = req.params.ids ? req.params.ids.split(',') : [];
            const data = await players.getStatsByIds(ids);
            res.send( data );
        } catch (err) {
            if (err === 'unauthorised') {
                res.status( 401 ).send( 'Unauthorised' );
            } else {
                res.status( 500 ).send( err.toString() );
            }
        }
});

module.exports = router;