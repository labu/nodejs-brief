const privates = {
    results: null,
    teams: null,
    players: null
}

const cache = {
    set results(data) {
        privates.results = data;
    },
    get results() {
        return privates.results;
    },
    set teams(data) {
        privates.teams = data;
    },
    get teams() {
        return privates.teams;
    },
    set players(data) {
        privates.players = data;
    },
    get players() {
        return privates.players;
    }
}

module.exports = cache;