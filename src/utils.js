const validateKey = (key) => {
    return key && key === process.env.LEAGUE_API_KEY;
}

module.exports = {
    validateKey
};