const axios = require('axios');
const cache = require('./cache')

const getAll = () => {
    if (cache.results) {
        return cache.results;
    }
    return axios.get( process.env.LEAGUE_SOURCE_ROOT_URL + '/results.json', {
        headers: {
            'x-api-key': process.env.LEAGUE_API_KEY
        }
    })
    .then( (res) => {
        cache.results = res.data;
        return res.data;
    });
}

module.exports = {
    getAll
};