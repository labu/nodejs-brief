const axios = require('axios');
const cache = require('./cache');
const results = require('./results');
const teams = require('./teams');

const getAll = () => {
    if (cache.players) {
        return cache.players;
    }
    return axios.get( process.env.LEAGUE_SOURCE_ROOT_URL + '/players.json', {
        headers: {
            'x-api-key': process.env.LEAGUE_API_KEY
        }
    })
    .then( (res) => {
        cache.players = res.data;
        return res.data;
    });
}

const getByIds = async (ids) => {
    const players = await getAll();
    if (ids.includes('*')) {
        console.log('returning all');
        return players;
    }
    return ids
        .map( (id) => players.find( (player) => player.player_id === id) )
        .filter( (item) => item );
}

// Finds all the games played by a player
const findGamesPlayed = (resultsData, playerId) => {
    return resultsData.reduce( (count, game) => {
        const players = [...game.home_team.players, ...game.visiting_team.players]
            .map( (p) => p.player_id );
        return players.includes(playerId) ? count + 1 : count;
    }, 0);
}

// Finds all the points scored by a player
const findPointsScored = (resultsData, playerId) => {
    return resultsData.reduce( (count, game) => {
        const homeMatch = game.home_team.players.find( (player) => player.player_id === playerId );
        if (homeMatch) {
            return count + homeMatch.points_scored;
        }
        const awayMatch = game.visiting_team.players.find( (player) => player.player_id === playerId );
        if (awayMatch) {
            return count + awayMatch.points_scored;
        }
        return count;
    }, 0);
}

const findTeamName = (teams, teamId) => {
    const match = teams.find( (team) => team.team_id === teamId );
    return match ? match.name : '';
}

const getStatsByIds = async (ids) => {
    console.log('ids', ids);
    const players = await getByIds(ids);
    const resultData = await results.getAll();
    const teamData = await teams.getAll();

    return players.map( (player) => {
        return Object.assign({}, player, {
            team_name: findTeamName(teamData, player.team_id),
            games_played: findGamesPlayed(resultData, player.player_id),
            points_scored: findPointsScored(resultData, player.player_id)
        }) 
    });
}

module.exports = {
    getAll,
    getByIds,
    getStatsByIds
};