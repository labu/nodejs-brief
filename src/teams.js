const axios = require('axios');
const cache = require('./cache');
const results = require('./results');

const getAll = () => {
    if (cache.teams) {
        return cache.teams;
    }
    return axios.get( process.env.LEAGUE_SOURCE_ROOT_URL + '/teams.json', {
        headers: {
            'x-api-key': process.env.LEAGUE_API_KEY
        }
    })
    .then( (res) => {
        cache.teams = res.data;
        return res.data;
    });
}

const getByIds = async (ids) => {
    const teams = await getAll();
    if (ids.includes('*')) {
        return teams;
    }
    return ids
        .map( (id) => teams.find( (team) => team.team_id === id) )
        .filter( (item) => item );
}

// Get Home Games
const getHomeGamesByTeamId = (results, id) => {
    return results.filter( (game) => game.home_team.team_id === id );
}

// Get Away Games
const getAwayGamesByTeamId = (results, id) => {
    return results.filter( (game) => game.visiting_team.team_id === id );
}

// Find the number of goals scored
const getGoalsScored = (players) => {
    return players.reduce( (goals, player) => goals += player.points_scored, 0 );
}

// Find the result of the game - main is the team the result is given to
const getResult = (main, opposition) => {
    const mainTeamScore = getGoalsScored(main.players);
    const oppositionTeamScore = getGoalsScored(opposition.players);
    return mainTeamScore > oppositionTeamScore ? 'win' : ( oppositionTeamScore > mainTeamScore ? 'loss' : 'draw' );
}

// Get Number of Win/Draws/Losses at chosen location
const getCountByResult = (games, result, location) => {
    return games.reduce( (count, game) => {
        const gameResult = location === 'home' ? getResult(game.home_team, game.visiting_team) : getResult(game.visiting_team, game.home_team);
        return gameResult === result ? count + 1 : count;
    }, 0);
}

// Gets the number of points scored in all away and home matches
const getPointsScored = (homeGames, awayGames) => {
    const homeGoals = homeGames.reduce( (goals, game) => {
        const goalsInGame = getGoalsScored(game.home_team.players);
        return goals += goalsInGame;
    }, 0);
    const awayGoals = awayGames.reduce( (goals, game) => {
        const goalsInGame = getGoalsScored(game.visiting_team.players);
        return goals += goalsInGame;
    }, 0);
    return homeGoals + awayGoals;
}

const getStatsByIds = async (ids) => {
    const teams = await getByIds(ids);
    const resultData = await results.getAll();

    return teams.map( (team) => {
        const homeGames = getHomeGamesByTeamId(resultData, team.team_id);
        const awayGames = getAwayGamesByTeamId(resultData, team.team_id);

        return Object.assign({}, team, {
            home_wins: getCountByResult(homeGames, 'win', 'home'),
            home_losses: getCountByResult(homeGames, 'loss', 'home'),
            home_draws: getCountByResult(homeGames, 'draw', 'home'),
            away_wins: getCountByResult(awayGames, 'win', 'away'),
            away_losses: getCountByResult(awayGames, 'loss', 'away'),
            away_draws: getCountByResult(awayGames, 'draw', 'away'),
            points_scored: getPointsScored(homeGames, awayGames),
            games_played: homeGames.length + awayGames.length
        }) 
    });
}

module.exports = {
    getAll,
    getByIds,
    getStatsByIds
};