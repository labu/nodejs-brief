const express = require( 'express' );
const cors = require( 'cors' );
const dotenv = require('dotenv');
dotenv.config();

const app = express();
const router = express.Router();
const port = process.env.LEAGUE_SERVER_PORT || 8099;

router.use( '/', require( './routing/api.router' ) );

app.use( cors() );
app.use( router );

app.listen( port, () => console.log(`Application running on port: ${port}`) );
